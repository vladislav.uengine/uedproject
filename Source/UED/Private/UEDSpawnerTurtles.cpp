// Fill out your copyright notice in the Description page of Project Settings.


#include "UEDSpawnerTurtles.h"

// Sets default values
AUEDSpawnerTurtles::AUEDSpawnerTurtles()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	SpawnerMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SpawnerMesh"));
	SpawnerMesh->SetupAttachment(Root);
	
}

// Called when the game starts or when spawned
void AUEDSpawnerTurtles::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AUEDSpawnerTurtles::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

