// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/UEDPawnBase.h"

// Sets default values
AUEDPawnBase::AUEDPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

// Called when the game starts or when spawned
void AUEDPawnBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AUEDPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AUEDPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

