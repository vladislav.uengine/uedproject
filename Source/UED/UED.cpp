// Copyright Epic Games, Inc. All Rights Reserved.

#include "UED.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UED, "UED" );
