// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "UEDTurtlePlayerControllerBase.generated.h"

/**
 * 
 */
UCLASS()
class UED_API AUEDTurtlePlayerControllerBase : public APlayerController
{
	GENERATED_BODY()
	
};
