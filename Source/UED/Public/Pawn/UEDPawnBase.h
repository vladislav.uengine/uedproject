// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DefaultPawn.h"
#include "NiagaraComponent.h"
#include "Data.h"
#include "UEDPawnBase.generated.h"

class UStaticMeshComponent;
class USceneComponent;
class UNiagaraSystem;
class USoundBase;

UCLASS()
class UED_API AUEDPawnBase : public ADefaultPawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AUEDPawnBase();

	//UPROPERTY()
	//USceneComponent* Root;
	//UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	//UStaticMeshComponent* TurtleMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn Turtle")
	USoundBase* SpawnSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawn Turtle")
	UNiagaraSystem* SpawnSystem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Walk Turtle")
	USoundBase* WalkSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Walk Turtle")
	UNiagaraSystem* WalkSystem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "End Road Turtle")
	USoundBase* EndRoadSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "End Road Turtle")
	UNiagaraSystem* EndRoadSystem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ENestType> NestType;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//UFUNCTION(BlueprintNativeEvent)
	//void MoveToNest(ENestType);

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	
};
