// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data.h"
#include "GameFramework/Actor.h"
#include "UEDSpawnerTurtles.generated.h"

class UStaticMeshComponent;
class USceneComponent;

UCLASS()
class UED_API AUEDSpawnerTurtles : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUEDSpawnerTurtles();

	UPROPERTY()
	USceneComponent* Root;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* SpawnerMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<ENestType> NestType;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
