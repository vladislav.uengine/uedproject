// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Data.Generated.h"

UENUM(BlueprintType, Category = "GameRules")
enum ENestType
{
	ENT_Nest_1 UMETA(DisplayName = "Nest_1"),
	ENT_Nest_2 UMETA(DisplayName = "Nest_2"),
	ENT_Nest_3 UMETA(DisplayName = "Nest_3")
};
