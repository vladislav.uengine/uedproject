// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UEDGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UED_API AUEDGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
